import pokedex from './gen1.json' assert {type: 'json'}; 

console.log(pokedex.length);

function addImg(){
    const pokemon0 = document.getElementById("pokemon0");
    const pokemon1 = document.getElementById("pokemon1");
    const pokemon2 = document.getElementById("pokemon2");
    const pokemon3 = document.getElementById("pokemon3");
    const pokemon4 = document.getElementById("pokemon4");
    const pokemon5 = document.getElementById("pokemon5");
    const p1 = document.getElementById("p1");
    const p2 = document.getElementById("p2");
    const p3 = document.getElementById("p3");
    const p4 = document.getElementById("p4");
    const p5 = document.getElementById("p5");
    const p6 = document.getElementById("p6");
    let types1 = document.getElementById("types1");
    let types2 = document.getElementById("types2");
    let types3 = document.getElementById("types3");
    let types4 = document.getElementById("types4");
    let types5 = document.getElementById("types5");
    let types6 = document.getElementById("types6");
    const diff = 150-1;
    let randomPokemonArray = [];
    let pokemonImg = [];
    let pokemonNames = [];
    let pokemonTypes = [];
    let counter = 0;

    do{
        let randomNum = Math.floor(Math.random() * (diff + 1)) + 1;
        randomPokemonArray.push(randomNum);
        counter++;
    }while(counter < 6);
    console.log(randomPokemonArray);
    for(let i = 0; i < randomPokemonArray.length; i++){
        for (let j = 0; j < pokedex.length; j++) {
            if(randomPokemonArray[i] === pokedex[j].pokedex){
                pokemonImg.push(pokedex[j].img);
                pokemonNames.push(pokedex[j].name);
                pokemonTypes.push(JSON.stringify(pokedex[j].types));
            } 
        }
    }
    console.log(pokemonTypes);
    pokemon0.src = pokemonImg[0];
    pokemon1.src = pokemonImg[1];
    pokemon2.src = pokemonImg[2];
    pokemon3.src = pokemonImg[3];
    pokemon4.src = pokemonImg[4];
    pokemon5.src = pokemonImg[5];
    
    p1.innerHTML = pokemonNames[0];
    p2.innerHTML = pokemonNames[1];
    p3.innerHTML = pokemonNames[2];
    p4.innerHTML = pokemonNames[3];
    p5.innerHTML = pokemonNames[4];
    p6.innerHTML = pokemonNames[5];

    types1.innerHTML = pokemonTypes[0].replace(/\s*"[^"]+"\s*:/g,"");
    types2.innerHTML = pokemonTypes[1].replace(/\s*"[^"]+"\s*:/g,"");
    types3.innerHTML = pokemonTypes[2].replace(/\s*"[^"]+"\s*:/g,"");
    types4.innerHTML = pokemonTypes[3].replace(/\s*"[^"]+"\s*:/g,"");
    types5.innerHTML = pokemonTypes[4].replace(/\s*"[^"]+"\s*:/g,"");
    types6.innerHTML = pokemonTypes[5].replace(/\s*"[^"]+"\s*:/g,"");
}

//.replace(/\s*"[^"]+"\s*:/g,"")
//use an array to save 6 random numbers, then iterate through said array
//to get the pokedex img
addImg();
