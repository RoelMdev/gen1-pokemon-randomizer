let pokeball1 = document.getElementById("pokeball1");
let pokeball2 = document.getElementById("pokeball2");
let pokeball3 = document.getElementById("pokeball3");
let pokeball4 = document.getElementById("pokeball4");
let pokeball5 = document.getElementById("pokeball5");
let pokeball6 = document.getElementById("pokeball6");
let dot1 = document.getElementById("dot1");
let dot2 = document.getElementById("dot2");
let dot3 = document.getElementById("dot3");
let dot4 = document.getElementById("dot4");
let dot5 = document.getElementById("dot5");
let dot6 = document.getElementById("dot6");
let p1 = document.getElementById("p1");
let p2 = document.getElementById("p2");
let p3 = document.getElementById("p3");
let p4 = document.getElementById("p4");
let p5 = document.getElementById("p5");
let p6 = document.getElementById("p6");
let types1 = document.getElementById("types1");
let types2 = document.getElementById("types2");
let types3 = document.getElementById("types3");
let types4 = document.getElementById("types4");
let types5 = document.getElementById("types5");
let types6 = document.getElementById("types6");
let button = document.getElementById("button");
//pokeball1 and dot 1 mouse over events
pokeball1.addEventListener('mouseover', () => {
    dot1.style.animation = "blink .8s alternate infinite";
});

dot1.addEventListener('mouseover', () => {
    dot1.style.animation = "blink .8s alternate infinite";
});

pokeball1.addEventListener('mouseout', () => {
    dot1.style.animation = "none";
});

dot1.addEventListener('mouseout', () => {
    dot1.style.animation = "none";
});

//click pokeball1 events
pokeball1.addEventListener('click', () => {
    dot1.style.animation = "blink 4s, grow 3s";
});

pokeball1.addEventListener('click', () => setTimeout(function(){
    p1.style.visibility = "visible";
    types1.style.visibility = "visible";
},3000));

pokeball1.addEventListener('click', () => setTimeout(function(){
    pokeball1.style.visibility = "hidden";
},1000));

pokeball1.addEventListener('click', () => setTimeout(function(){
    dot1.style.visibility = "hidden";
},3000));

//Pokeball2 
//pokeball2 and dot 2 mouse over events
pokeball2.addEventListener('mouseover', () => {
    dot2.style.animation = "blink .8s alternate infinite";
});

dot2.addEventListener('mouseover', () => {
    dot2.style.animation = "blink .8s alternate infinite";
});

pokeball2.addEventListener('mouseout', () => {
    dot2.style.animation = "none";
});

dot2.addEventListener('mouseout', () => {
    dot2.style.animation = "none";
});

//click pokeball2 events
pokeball2.addEventListener('click', () => {
    dot2.style.animation = "blink 4s, grow 3s";
});

pokeball2.addEventListener('click', () => setTimeout(function(){
    p2.style.visibility = "visible";
    types2.style.visibility = "visible";
},3000));

pokeball2.addEventListener('click', () => setTimeout(function(){
    pokeball2.style.visibility = "hidden";
},1000));

pokeball2.addEventListener('click', () => setTimeout(function(){
    dot2.style.visibility = "hidden";
},3000));

//Pokeball3
//pokeball3 and dot 3 mouse over events
pokeball3.addEventListener('mouseover', () => {
    dot3.style.animation = "blink .8s alternate infinite";
});

dot3.addEventListener('mouseover', () => {
    dot3.style.animation = "blink .8s alternate infinite";
});

pokeball3.addEventListener('mouseout', () => {
    dot3.style.animation = "none";
});

dot3.addEventListener('mouseout', () => {
    dot3.style.animation = "none";
});

//click pokeball3 events
pokeball3.addEventListener('click', () => {
    dot3.style.animation = "blink 4s, grow 3s";
});

pokeball3.addEventListener('click', () => setTimeout(function(){
    p3.style.visibility = "visible";
    types3.style.visibility = "visible";
},3000));

pokeball3.addEventListener('click', () => setTimeout(function(){
    pokeball3.style.visibility = "hidden";
},1000));

pokeball3.addEventListener('click', () => setTimeout(function(){
    dot3.style.visibility = "hidden";
},3000));

//Pokeball4
//pokeball4 and dot 4 mouse over events
pokeball4.addEventListener('mouseover', () => {
    dot4.style.animation = "blink .8s alternate infinite";
});

dot4.addEventListener('mouseover', () => {
    dot4.style.animation = "blink .8s alternate infinite";
});

pokeball4.addEventListener('mouseout', () => {
    dot4.style.animation = "none";
});

dot4.addEventListener('mouseout', () => {
    dot4.style.animation = "none";
});

//click pokeball4 events
pokeball4.addEventListener('click', () => {
    dot4.style.animation = "blink 4s, grow 3s";
});

pokeball4.addEventListener('click', () => setTimeout(function(){
    p4.style.visibility = "visible";
    types4.style.visibility = "visible";
},3000));

pokeball4.addEventListener('click', () => setTimeout(function(){
    pokeball4.style.visibility = "hidden";
},1000));

pokeball4.addEventListener('click', () => setTimeout(function(){
    dot4.style.visibility = "hidden";
},3000));

//Pokeball5
//pokeball5 and dot 5 mouse over events
pokeball5.addEventListener('mouseover', () => {
    dot5.style.animation = "blink .8s alternate infinite";
});

dot5.addEventListener('mouseover', () => {
    dot5.style.animation = "blink .8s alternate infinite";
});

pokeball5.addEventListener('mouseout', () => {
    dot5.style.animation = "none";
});

dot5.addEventListener('mouseout', () => {
    dot5.style.animation = "none";
});

//click pokeball5 events
pokeball5.addEventListener('click', () => {
    dot5.style.animation = "blink 4s, grow 3s";
});

pokeball5.addEventListener('click', () => setTimeout(function(){
    p5.style.visibility = "visible";
    types5.style.visibility = "visible";
},3000));

pokeball5.addEventListener('click', () => setTimeout(function(){
    pokeball5.style.visibility = "hidden";
},1000));

pokeball5.addEventListener('click', () => setTimeout(function(){
    dot5.style.visibility = "hidden";
},3000));

//Pokeball6
//pokeball6 and dot 6 mouse over events
pokeball6.addEventListener('mouseover', () => {
    dot6.style.animation = "blink .8s alternate infinite";
});

dot6.addEventListener('mouseover', () => {
    dot6.style.animation = "blink .8s alternate infinite";
});

pokeball6.addEventListener('mouseout', () => {
    dot6.style.animation = "none";
});

dot6.addEventListener('mouseout', () => {
    dot6.style.animation = "none";
});

//click pokeball6 events
pokeball6.addEventListener('click', () => {
    dot6.style.animation = "blink 4s, grow 3s";
});

pokeball6.addEventListener('click', () => setTimeout(function(){
    p6.style.visibility = "visible";
    types6.style.visibility = "visible";
},3000));

pokeball6.addEventListener('click', () => setTimeout(function(){
    pokeball6.style.visibility = "hidden";
},1000));

pokeball6.addEventListener('click', () => setTimeout(function(){
    dot6.style.visibility = "hidden";
},3000));

//button
button.addEventListener('click', () =>{
    window.location.reload();
});